# Codding challenge
The app allows creating messages and message replies. The application runs against a mock server. It handles basic CRUD operations.
## Get started
```
npm install
```
Then rename the file `.env.dist` into `.env` and set the url in it as `http://localhost:8000`.
Launch the app:
```
npm run start
```
Launch mock server:
```
npm run server
```
