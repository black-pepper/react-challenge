import { Reply } from "./reply";
import { Edit } from "./edit";
import { Delete } from "./delete";
import { useState } from "react";
import { Spacer } from "./styled";

export const MessageActions = (props: { id: number; message: string }) => {
  const { id, message } = props;
  const [currentToggle, setCurrentToggle] = useState("");

  const handleCancel = () => setCurrentToggle("");

  return (
    <div>
      <div className="is-flex is-justify-content-flex-start">
        <button onClick={() => setCurrentToggle("reply")}>Reply</button>
        <Spacer />
        <button onClick={() => setCurrentToggle("edit")}>Edit</button>
        <Spacer />
        <button onClick={() => setCurrentToggle("delete")}>Delete</button>
      </div>
      <div>
        {currentToggle === "reply" && (
          <div
            className={
              currentToggle === "reply"
                ? "panel-block is-active"
                : "panel-block"
            }
          >
            <Reply label="Reply" parentId={id} handleCancel={handleCancel} />
          </div>
        )}
        {currentToggle === "edit" && (
          <div
            className={
              currentToggle === "edit" ? "panel-block is-active" : "panel-block"
            }
          >
            <Edit
              id={id}
              label="Edit"
              message={message}
              handleCancel={handleCancel}
            />
          </div>
        )}
        {currentToggle === "delete" && (
          <div
            className={
              currentToggle === "delete"
                ? "panel-block is-active"
                : "panel-block"
            }
          >
            <Delete id={id} handleCancel={handleCancel} />
          </div>
        )}
      </div>
    </div>
  );
};
