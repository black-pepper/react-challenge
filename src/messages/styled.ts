import styled from "styled-components";

export const MessageWrapper = styled.div`
  position: relative;
  margin-left: 1rem;
`;

export const Spacer = styled.span`
  margin: 0 1rem;
  &::before {
    content: "|";
  }
`;
