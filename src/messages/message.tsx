import { Message, StoreType } from "../store/types";
import { useSelector } from "react-redux";
import { MessageWrapper } from "./styled";
import { MessageActions } from "./message-actions";

const MessageItem = (props: Message) => {
  const { message, id } = props;
  const messages = useSelector(({ messages }: StoreType) => messages);
  return (
    <MessageWrapper>
      <div className="is-tabbed">
        <div>{message}</div>
        <MessageActions message={message} id={id} />
      </div>
      {messages
        .filter((message) => message.id !== id && message.parentId === id)
        .map((message) => (
          <MessageItem key={message.id} {...message} />
        ))}
    </MessageWrapper>
  );
};

export default MessageItem;
