import { MessageForm } from "./message-form";
import store from "../store";
import { UPDATE_MESSAGE } from "../store/action-types";
import { updateMessage } from "../services";

export const Edit = (props: {
  id: number;
  label: string;
  parentId?: number;
  message: string;
  handleCancel: () => void;
}) => {
  const { id, label, parentId, message, handleCancel } = props;
  const handleSubmit = (value: { message: string }) => {
    const { message } = value;

    updateMessage(id, { message, author: 0, parentId }).then(({ data }) => {
      store.dispatch({
        type: UPDATE_MESSAGE,
        payload: data,
      });
      handleCancel && handleCancel();
    });
  };

  return (
    <MessageForm
      currentValue={message}
      parentId={parentId}
      label={label}
      onSubmit={handleSubmit}
      handleCancel={handleCancel}
    />
  );
};
