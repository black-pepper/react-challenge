import { MessageForm } from "./message-form";
import store from "../store";
import { ADD_MESSAGE } from "../store/action-types";
import { addMessage } from "../services";

export const Reply = (props: {
  label: string;
  parentId?: number;
  handleCancel?: () => void;
}) => {
  const { label, parentId, handleCancel } = props;
  const handleSubmit = (value: { message: string }) => {
    const { message } = value;

    addMessage({ message, author: 0, parentId }).then(({ data }) => {
      store.dispatch({
        type: ADD_MESSAGE,
        payload: data,
      });
      handleCancel && handleCancel();
    });
  };

  return (
    <MessageForm
      parentId={parentId}
      label={label}
      onSubmit={handleSubmit}
      handleCancel={handleCancel}
    />
  );
};
