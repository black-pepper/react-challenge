import { ChangeEvent, FormEvent, useState } from "react";

export const MessageForm = (props: {
  onSubmit: (val: { message: string; parentId?: number }) => void;
  handleCancel?: () => void;
  label: string;
  parentId?: number;
  currentValue?: string;
}) => {
  const { handleCancel, onSubmit, label, currentValue } = props;
  const initialData = { message: currentValue || "" };
  const [data, setData] = useState(initialData);

  const handleChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setData({ message: e.target.value });
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (data.message !== "") {
      onSubmit({ ...data, parentId: props.parentId });
      setData(initialData);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <textarea
        className="textarea"
        onChange={handleChange}
        value={data.message}
        autoFocus={true}
      />
      <button
        className="button is-link is-outlined"
        type="submit"
        disabled={data.message === ""}
      >
        {label}
      </button>
      <button className="button is-link is-outlined" onClick={handleCancel}>
        Cancel
      </button>
    </form>
  );
};
