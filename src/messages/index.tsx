import { useSelector } from "react-redux";
import { MessagesListProps, StoreType } from "../store/types";
import MessageItem from "./message";
import { Fragment, useCallback, useEffect } from "react";
import { Reply } from "./reply";
import { getAllMessages } from "../services";
import store from "../store";
import { ADD_MESSAGES } from "../store/action-types";

const NoMessages = () => <div>No messages</div>;

const MessagesList = (props: MessagesListProps) => {
  const { messages } = props;
  return (
    <Fragment>
      {messages.map((message) => (
        <MessageItem key={message.id} {...message} />
      ))}
    </Fragment>
  );
};

const Messages = () => {
  const messages = useSelector(({ messages }: StoreType) => messages);

  const fetchMessages = useCallback(() => {
    getAllMessages()
      .then(({ data }) => store.dispatch({ type: ADD_MESSAGES, payload: data }))
      .catch((error) => {
        // Do something with the error
        throw Error(error);
      });
  }, []);

  useEffect(() => {
    fetchMessages();
  }, [fetchMessages]);

  const getParentMessages = () =>
    messages.filter((message) => !message.parentId);

  return (
    <nav className="panel">
      <p className="panel-heading">Messages</p>
      {messages.length > 0 ? (
        <MessagesList messages={getParentMessages()} />
      ) : (
        <NoMessages />
      )}
      <Reply label="Add a message" />
    </nav>
  );
};

export default Messages;
