import store from "../store";
import { REMOVE_MESSAGE } from "../store/action-types";
import { deleteMessage } from "../services";

export const Delete = (props: { id: number; handleCancel: () => void }) => {
  const { id, handleCancel } = props;

  const handleDelete = () => {
    deleteMessage(id).then(() => {
      store.dispatch({
        type: REMOVE_MESSAGE,
        payload: { id },
      });
    });
  };

  return (
    <div>
      Are you sure to delete this message ?{" "}
      <button className="button is-link is-outlined" onClick={handleDelete}>
        Yes
      </button>
      <button className="button is-link is-outlined" onClick={handleCancel}>
        No
      </button>
    </div>
  );
};
