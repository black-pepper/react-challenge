import React from "react";
import "./App.css";
import Messages from "./messages";

function App() {
  return (
    <div>
      <Messages />
    </div>
  );
}

export default App;
