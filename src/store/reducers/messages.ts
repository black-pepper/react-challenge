import {
  ADD_MESSAGE,
  ADD_MESSAGES,
  REMOVE_MESSAGE,
  UPDATE_MESSAGE,
} from "../action-types";
import { ActionType, Message } from "../types";

const initialState: Message[] = [];

export const messages = (state = initialState, action: ActionType) => {
  const { type, payload } = action;
  switch (type) {
    case ADD_MESSAGES:
      return Array.isArray(payload) && [...state, ...payload];
    case ADD_MESSAGE:
      return !Array.isArray(payload) && [...state, payload];
    case UPDATE_MESSAGE:
      return (
        !Array.isArray(payload) &&
        state.map((message) => (message.id === payload.id ? payload : message))
      );
    case REMOVE_MESSAGE:
      return (
        !Array.isArray(payload) &&
        state.filter((message) => message.id !== payload.id)
      );
    default:
      return state;
  }
};
