export const ADD_MESSAGES = "ADD_MESSAGES";
export const ADD_MESSAGE = "ADD_MESSAGE";
export const UPDATE_MESSAGE = "UPDATE_MESSAGE";
export const REMOVE_MESSAGE = "REMOVE_MESSAGE";
