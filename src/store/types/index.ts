import {
  ADD_MESSAGE,
  ADD_MESSAGES,
  REMOVE_MESSAGE,
  UPDATE_MESSAGE,
} from "../action-types";

export interface BaseMessage {
  message: string;
  author: number;
}

export interface Message extends BaseMessage {
  id: number;
  parentId?: number;
}

export type ActionType =
  | {
      type: typeof ADD_MESSAGES;
      payload: Message[];
    }
  | {
      type: typeof ADD_MESSAGE | typeof UPDATE_MESSAGE;
      payload: Message;
    }
  | {
      type: typeof REMOVE_MESSAGE;
      payload: { id: number };
    };

export interface StoreType {
  messages: Message[];
}

export interface MessagesListProps {
  messages: Message[];
}
