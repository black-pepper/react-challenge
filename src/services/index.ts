import axios from "axios";
import { Message } from "../store/types";

const instance = axios.create();

export const getAllMessages = () =>
  instance.get(`${process.env.REACT_APP_API}/messages`);

export const addMessage = (message: Partial<Message>) =>
  instance.post(`${process.env.REACT_APP_API}/messages`, message);

export const updateMessage = (id: number, message: Partial<Message>) =>
  instance.patch(`${process.env.REACT_APP_API}/messages/${id}`, message);

export const deleteMessage = (id: number) =>
  instance.delete(`${process.env.REACT_APP_API}/messages/${id}`);
